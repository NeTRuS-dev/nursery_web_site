﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Labs.Models
{
    /**
     * Реальное животное
     */
    public class Pet
    {
        public int Id { get; set; }
        [Required] [Display(Name = "Кличка")] public string Name { get; set; }
        [Display(Name = "Возраст")] public int Age { get; set; }

        [Required]
        [Display(Name = "Тип животного")]
        public int PetSubTypeId { get; set; }

        public virtual PetSubType PetSubType { get; set; }
        public virtual User Owner { get; set; }
        public string OwnerId { get; set; }

        public string GetFullTypeName()
        {
            return $"{this.PetSubType.PetType.Name}({this.PetSubType.Name})";
        }
    }
}