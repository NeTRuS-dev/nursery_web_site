﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Labs.Models
{
    /**
     * Порода животного
     */
    public class PetSubType
    {
        public int Id { get; set; }
        [Required] [DisplayName("Название")] public string Name { get; set; }
        [DisplayName("Описание")] public string Description { get; set; }
        public virtual List<Pet> Pets { get; set; }
        public virtual PetType PetType { get; set; }
        [DisplayName("Тип")] [Required] public int PetTypeId { get; set; }
    }
}