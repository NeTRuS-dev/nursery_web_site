﻿namespace Labs.Models.FormModels
{
    public class LoginForm
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}