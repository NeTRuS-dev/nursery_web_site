﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Labs.Models
{
    /**
     * Вид животного - собака/кошка
     */
    public class PetType
    {
        public int Id { get; set; }
        [Required] [DisplayName("Название")] public string Name { get; set; }
        [DisplayName("Описание породы")] public string Description { get; set; }
        public virtual List<PetSubType> PetSubTypes { get; set; }
    }
}