﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Labs.Models.DbContexts
{
    public class MainDbContext : IdentityDbContext<User, Role, string>
    {
        public DbSet<Pet> Pets { get; set; }
        public DbSet<PetSubType> PetSubTypes { get; set; }
        public DbSet<PetType> PetTypes { get; set; }

        public MainDbContext(DbContextOptions<MainDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // string даёт restrict удаление пришлось переопределять
            builder
                .Entity<Pet>()
                .HasOne<User>(p => p.Owner)
                .WithMany(u => u.MyPets)
                .HasForeignKey(p => p.OwnerId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);


            // связь ролей и пользователей
            builder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .UsingEntity<IdentityUserRole<string>>(
                    userRole => userRole.HasOne<Role>().WithMany().HasForeignKey(ur => ur.RoleId).IsRequired(),
                    userRole => userRole.HasOne<User>().WithMany().HasForeignKey(ur => ur.UserId).IsRequired());
        }
    }
}