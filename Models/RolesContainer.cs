﻿namespace Labs.Models
{
    public class RolesContainer
    {
        public const string Admin = "admin"; // админ сайта
        public const string Manager = "manager"; // менеджер по управлению питомником
        public const string User = "user"; // клиент
    }
}