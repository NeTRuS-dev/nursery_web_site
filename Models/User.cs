﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Castle.Components.DictionaryAdapter;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Labs.Models
{
    public class User : IdentityUser
    {
        [Required] public string FirstName { get; set; }
        [Required] public string Surname { get; set; }
        public string Patronymic { get; set; }
        [DefaultValue(0)] public decimal Balance { get; set; }
        [InverseProperty("Owner")] public virtual List<Pet> MyPets { get; set; }
        [Required] [DataType(DataType.Date)]public DateTime DateOfBirth { get; set; }
        [JsonInclude] public string FullName => $"{Surname} {FirstName} {Patronymic}".Trim();

        [InverseProperty("Users")] public virtual List<Role> Roles { get; set; }
    }
}