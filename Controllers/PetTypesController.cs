﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labs.Models;
using Labs.Models.DbContexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Labs.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PetTypesController : Controller
    {
        private readonly ILogger<PetTypesController> _logger;
        private readonly MainDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager<User> _signInManager;

        public PetTypesController(ILogger<PetTypesController> logger, MainDbContext dbContext,
            UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            _logger = logger;
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }


        [HttpPost("get-pet-types")]
        public async Task<IActionResult> GetPetTypes()
        {
            return Json(
                await this._dbContext.PetTypes.ToListAsync()
            );
        }


        [HttpPost("update-pet-type")]
        public async Task<IActionResult> UpdatePetType([FromBody]PetType type)
        {
            List<string> messages = null;
            bool status = true;
            if (ModelState.IsValid)
            {
                try
                {
                    if (type.Id == default)
                    {
                        await this._dbContext.PetTypes.AddAsync(type);
                    }
                    else
                    {
                        this._dbContext.PetTypes.Update(type);
                    }

                    await this._dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    status = false;
                    messages = new List<string>() {e.Message};
                }
            }

            {
                messages = ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage).ToList();
            }
            return Json(new
            {
                status = status,
                messages = messages,
            });
        }


        [HttpPost("delete-pet-type/:id")]
        public async Task<IActionResult> DeletePetType(int id)
        {
            var record = await this._dbContext.PetTypes.FindAsync(id);
            List<string> messages = null;
            bool status = true;
            if (record is not null)
            {
                try
                {
                    this._dbContext.PetTypes.Remove(record);
                    await this._dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    status = false;
                    messages = new List<string>() {e.Message};
                }
            }

            return Json(new
            {
                status,
                messages
            });
        }
    }
}