﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labs.Models;
using Labs.Models.DbContexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Labs.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PetSubTypesController:Controller
    {
        private readonly ILogger<PetSubTypesController> _logger;
        private readonly MainDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager<User> _signInManager;

        public PetSubTypesController(ILogger<PetSubTypesController> logger, MainDbContext dbContext,
            UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            _logger = logger;
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }

        [HttpPost("get-pet-subtypes")]
        public async Task<IActionResult> GetPetSubTypes()
        {
            return Json(
                await this._dbContext.PetSubTypes.Include(sub => sub.PetType).ToListAsync()
            );
        }
        
        [HttpPost("update-pet-subtype")]
        public async Task<IActionResult> UpdatePetSubType([FromBody]PetSubType subType)
        {
            List<string> messages = null;
            bool status = true;
            if (ModelState.IsValid)
            {
                try
                {
                    if (subType.Id == default)
                    {
                        await this._dbContext.PetSubTypes.AddAsync(subType);
                    }
                    else
                    {
                        this._dbContext.PetSubTypes.Update(subType);
                    }

                    await this._dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    status = false;
                    messages = new List<string>() {e.Message};
                }
            }
            else
            {
                messages = ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage).ToList();
            }

            return Json(new
            {
                status = status,
                messages = messages,
            });
        }
        
        [HttpPost("delete-pet-subtype/:id")]
        public async Task<IActionResult> DeletePetSubType(int id)
        {
            var record = await this._dbContext.PetSubTypes.FindAsync(id);
            List<string> messages = null;
            bool status = true;
            if (record is not null)
            {
                try
                {
                    this._dbContext.PetSubTypes.Remove(record);
                    await this._dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    status = false;
                    messages = new List<string>() {e.Message};
                }
            }

            return Json(new
            {
                status,
                messages
            });
        }
        
        
    }
}