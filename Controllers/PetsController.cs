﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labs.Models;
using Labs.Models.DbContexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Labs.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PetsController : Controller
    {
        private readonly ILogger<PetsController> _logger;
        private readonly MainDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager<User> _signInManager;

        public PetsController(ILogger<PetsController> logger, MainDbContext dbContext,
            UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            _logger = logger;
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }


        [HttpPost("get-pets")]
        public async Task<IActionResult> GetPets()
        {
            return Json(
                await this._dbContext.Pets
                    .Include(pet => pet.PetSubType)
                    .ThenInclude(sub => sub.PetType)
                    .ToListAsync()
            );
        }
        
        
        [HttpPost("update-pet")]
        public async Task<IActionResult> UpdatePet([FromBody]Pet pet)
        {
            List<string> messages = null;
            bool status = true;
            if (ModelState.IsValid)
            {
                try
                {
                    if (pet.Id == default)
                    {
                        await this._dbContext.Pets.AddAsync(pet);
                    }
                    else
                    {
                        this._dbContext.Pets.Update(pet);
                    }

                    await this._dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    status = false;
                    messages = new List<string>() {e.Message};
                }
            }
            else
            {
                messages = ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage).ToList();
            }

            return Json(new
            {
                status = status,
                messages = messages,
            });
        }
        
        
        [HttpPost("delete-pet/:id")]
        public async Task<IActionResult> DeletePet(int id)
        {
            var record = await this._dbContext.Pets.FindAsync(id);
            List<string> messages = null;
            bool status = true;
            if (record is not null)
            {
                try
                {
                    this._dbContext.Pets.Remove(record);
                    await this._dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    status = false;
                    messages = new List<string>() {e.Message};
                }
            }

            return Json(new
            {
                status,
                messages
            });
        }
        
    }
}